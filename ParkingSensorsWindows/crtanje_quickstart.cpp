#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>

#include "GL/glut.h"
#include "../../sndfile.hh"


#include <math.h>
#include <iostream>

#include <windows.h>
#include <mmsystem.h>
#include <io.h>


#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <cstdlib>
#include<minmax.h>

using namespace std;
void display(void);
void reshape(int, int);
void idle();
void readSensors(unsigned char, int, int);
void draw_rectangle();
void draw_background();
int zvuk = 0;
int sensor_state[4];
bool direction_forward = true;
bool sound_played = false;
const char* L1 = "..\\Sounds\\L-1.wav";
const char* L2 = "..\\Sounds\\L-2.wav";
const char* LR1 = "..\\Sounds\\LR-1.wav";
const char* LR2 = "..\\Sounds\\LR-2.wav";
const char* R1 = "..\\Sounds\\R-1.wav";
const char* R2 = "..\\Sounds\\R-2.wav";


unsigned char* loadPPM(const char* filename, int& width, int& height) {
	const int BUFSIZE = 128;
	FILE* fp;
	unsigned int read;
	unsigned char* rawData;
	char buf[3][BUFSIZE];
	char* retval_fgets;
	size_t retval_sscanf;
	if ((fp = fopen(filename, "rb")) == NULL)
	{
		std::cerr << "error reading ppm file, could not locate " << filename << std::endl;
		width = 0;
		height = 0;
		return NULL;
	}
	retval_fgets = fgets(buf[0], BUFSIZE, fp);
	do
	{
		retval_fgets = fgets(buf[0], BUFSIZE, fp);
	} while (buf[0][0] == '#');
	retval_sscanf = sscanf(buf[0], "%s %s", buf[1], buf[2]);
	width = atoi(buf[1]);
	height = atoi(buf[2]);
	do
	{
		retval_fgets = fgets(buf[0], BUFSIZE, fp);
	} while (buf[0][0] == '#');
	rawData = new unsigned char[width * height * 3];
	read = fread(rawData, width * height * 3, 1, fp);
	fclose(fp);
	if (read != 1)
	{
		std::cerr << "error parsing ppm file, incomplete data" << std::endl;
		delete[] rawData;
		width = 0;
		height = 0;
		return NULL;
	}
	return rawData;
}

void initGL()
{
	glEnable(GL_TEXTURE_2D); // enable texture mapping
	glShadeModel(GL_SMOOTH); // enable smooth shading
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // get clear background (black color)
	glClearDepth(1.0f); // color depth buffer
	glDepthFunc(GL_LEQUAL); // configuration of depth testing
	//enable additional options regarding: perspective correction, anti-aliasing, etc
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

void loadTexture()
{
	GLuint texture[1]; // declaring space for one texture
	int twidth, theight; // declaring variable for width and height of an image
	unsigned char* tdata; // declaring pixel data
	// loading image data from specific file:
	tdata = loadPPM("..\\auto3.ppm", twidth, theight);
	if (tdata == NULL) return; // check if image data is loaded
	// generating a texture to show the image
	glGenTextures(1, &texture[0]);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE,
		tdata);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

int main(int argc, char** argv) {
	/* 1) INITIALIZATION */
	// initialize GLUT
	glutInit(&argc, argv);
	// set window position and size
	glutInitWindowPosition(545, 180);
	glutInitWindowSize(720, 720);
	// set the combination of predefined values for display mode
	// set color space (Red, Green, Blue - RGB)
	// alocate depth buffer
	// set the size of the buffer (double)
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	// create window
	glutCreateWindow("Screen");
	/* 2) REGISTRATION OF CALLBACK FUNCTION */
	// function called when new window need to be drawn
	glutDisplayFunc(display);
	// function called when window changes the size
	glutReshapeFunc(reshape);
	// function called when nothing else is executing and CPU is free
	glutIdleFunc(idle);
	// function called when keyboard key is pressed
	glutKeyboardFunc(readSensors); // custom function 'readSensors' can	be implemented separately
	loadTexture();
	initGL();
	/* 3) START GLUT PROCESSING CYCLE */
	glutMainLoop();
	return 0;
}

void draw_background() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBegin(GL_QUADS);

	// drawing a background white rectangle
	glColor3f(1, 1, 1); // choosing a color
	glTexCoord2f(0, 1); glVertex3f(-2, -1, 0);
	glTexCoord2f(1, 1); glVertex3f(2, -1, 0);
	glTexCoord2f(1, 0); glVertex3f(2, 1, 0);
	glTexCoord2f(0, 0); glVertex3f(-2, 1, 0);
}

void draw_rectangle() {
	draw_background();

	short x = 1, y = 1;

	for (int i = 0; i < 4; i++) {
		if (i == 0)
			x = -1;
		else if (i == 1) {
			x = 1;
			y = 1;
		}
		else if (i == 2)
			y = -1;
		else if (i == 3) {
			x = -1;
			y = -1;
		}

		if (sensor_state[i] >= 1) {
			// drawing a rectangle 1 
			glColor3f(1.0, 0.50, 0.50);
			glVertex3f(1.48f * x, 0.21f * y, 0.0f);
			glVertex3f(1.40f * x, 0.16f * y, 0.0f);
			glVertex3f(1.14f * x, 0.55f * y, 0.0f);
			glVertex3f(1.22f * x, 0.60f * y, 0.0f);
		}
		if (sensor_state[i] >= 2) {
			// drawing a rectangle 2
			glColor3f(1.0, 0.35, 0.35);
			glVertex3f(1.35f * x, 0.20f * y, 0.00f);
			glVertex3f(1.28f * x, 0.16f * y, 0.0f);
			glVertex3f(1.09f * x, 0.46f * y, 0.00f);
			glVertex3f(1.15f * x, 0.50f * y, 0.0f);
		}
		if (sensor_state[i] == 3) {
			// drawing a rectangle 3
			glColor3f(1.0, 0.0, 0.0);
			glVertex3f(1.22f * x, 0.20f * y, 0.0f);
			glVertex3f(1.15f * x, 0.16f * y, 0.0f);
			glVertex3f(1.03f * x, 0.36f * y, 0.0f);
			glVertex3f(1.1f * x, 0.40f * y, 0.0f);
		}
	}

	glEnd();
	glutSwapBuffers();

	return;
}

void readSensors(unsigned char key, int x, int y) {
	switch (key) {
	case 'e':
		sensor_state[0] = 3;
		break;
	case 'w':
		sensor_state[0] = 2;
		break;
	case 'q':
		sensor_state[0] = 1;
		break;
	case 'r':
		sensor_state[0] = 0;
		display();
		break;
	case 'u':
		sensor_state[1] = 3;
		break;
	case 'z':;
		sensor_state[1] = 2;
		break;
	case 't':
		sensor_state[1] = 1;
		break;
	case 'i':
		sensor_state[1] = 0;
		display();
		break;
	case 'j':
		sensor_state[2] = 3;
		break;
	case 'h':
		sensor_state[2] = 2;
		break;
	case 'g':
		sensor_state[2] = 1;
		break;
	case 'k':
		sensor_state[2] = 0;
		display();
		break;
	case 'd':
		sensor_state[3] = 3;
		break;
	case 's':
		sensor_state[3] = 2;
		break;
	case 'a':
		sensor_state[3] = 1;
		break;
	case 'f':
		sensor_state[3] = 0;
		display();
		break;
	case 'y':
		direction_forward = true;
		break;
	case 'x':
		direction_forward = false;
		break;
	}

	draw_rectangle();
}

void display() {
	cerr << "display callback" << endl;
	// clean color buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// start drawing quads
	glBegin(GL_QUADS);
	// choose color (white)
	glColor3f(1, 1, 1);
	// coordinates of initial white rectangle for the background
	glTexCoord2f(0, 1); glVertex3f(-2, -1, 0);
	glTexCoord2f(1, 1); glVertex3f(2, -1, 0);
	glTexCoord2f(1, 0); glVertex3f(2, 1, 0);
	glTexCoord2f(0, 0); glVertex3f(-2, 1, 0);
	// end of drawing
	glEnd();
	// swap buffers to show new graphics
	glutSwapBuffers();
}

void reshape(int width, int height)
{
	cerr << "reshape callback" << endl;
	// specify the desired rectangle
	glViewport(0, 0, width, height);
	// switch to matrix projection
	glMatrixMode(GL_PROJECTION);
	// clean projection matrix
	glLoadIdentity();
	// set camera view (orthographic projection with 4x4 unit square canvas)
	glOrtho(-2, 2, -2, 2, 2, -2);
	// swith back to matrix
	glMatrixMode(GL_MODELVIEW);
}

void idle()
{
	if (direction_forward && (sensor_state[0] != 0 || sensor_state[3] != 0)) {
		if (sensor_state[0] > sensor_state[3]) {
			PlaySound(R1, NULL, SND_SYNC);
			if (sensor_state[0] == 1) Sleep(350);
			if (sensor_state[0] == 2) Sleep(200);
		}		
		else if (sensor_state[0] < sensor_state[3]) {
			PlaySound(L1, NULL, SND_SYNC);
			if (sensor_state[3] == 1) Sleep(350);
			if (sensor_state[3] == 2) Sleep(200);
		}
		else if (sensor_state[0] == sensor_state[3])
		{
			PlaySound(LR1, NULL, SND_SYNC);
			if (sensor_state[3] == 1) Sleep(350);
			if (sensor_state[3] == 2) Sleep(200);
		}
	}
	else if (!direction_forward && (sensor_state[1] != 0 || sensor_state[2] != 0)) {
		if (sensor_state[1] > sensor_state[2]) {
			PlaySound(R2, NULL, SND_SYNC);
			if (sensor_state[1] == 1) Sleep(400);
			if (sensor_state[1] == 2) Sleep(250);
		}
		else if (sensor_state[1] < sensor_state[2]) {
			PlaySound(L2, NULL, SND_SYNC);
			if (sensor_state[1] == 1) Sleep(500);
			if (sensor_state[1] == 2) Sleep(250);
		}
		else if (sensor_state[1] == sensor_state[2])
		{
			PlaySound(LR2, NULL, SND_SYNC);
			if (sensor_state[2] == 1) Sleep(500);
			if (sensor_state[2] == 2) Sleep(250);
		}
	}
}